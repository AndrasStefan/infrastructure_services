#!/usr/bin/env bash

# default konga user/pass: admin/adminadminadmin

# example ssh tunnel for staging through dmi-test:
# ssh -NL 8445:api-gw.staging.nationalgeographic.org:444 dmi-test
# example ssh tunnel for production through dmi-test:
# ssh -NL 8444:api-gw.nationalgeographic.org:444 dmi-test

# with the above tunnels, create the following connections in konga:
# staging: https://host.docker.internal:8444
# production: https://host.docker.internal:8445

# for this command, the following assumptions are made:
# a database exists for konga and the docker host exposes it at localhost:5432
docker run --rm \
  -p 1337:1337 \
  -e "NODE_TLS_REJECT_UNAUTHORIZED=0" \
  -e "TOKEN_SECRET=1234" \
  -e "DB_ADAPTER=postgres" \
  -e "DB_HOST=host.docker.internal" \
  -e "DB_PORT=5432" \
  -e "DB_DATABASE=konga" \
  -e "DB_USER=postgres" \
  -e "DB_PASSWORD=postgres" \
  -e "NODE_ENV=development" \
  --name konga \
  pantsel/konga:legacy


# tips:
# shutdown with: docker stop konga
# an external database is not necessary. The Konga container can run with a local db instead.
# See https://hub.docker.com/r/pantsel/konga/ for more info
